import os
oidc_secret = os.environ.get('OIDC_SECRET')
proxy_url = os.environ.get('PROXY_URL')
iodc_client_secrets_path = os.environ.get('IODC_CLIENT_SECRETS_PATH')
keycloak_realm = os.environ.get('KEYCLOAK_REALM')

import flask
from flask_oidc import OpenIDConnect
import requests
import re
import json
import urllib.parse
import keycloak_admin

ka = keycloak_admin.KeycloakAdmin()

app = flask.Flask(__name__)

app.config.update({
    'SECRET_KEY': oidc_secret,
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': iodc_client_secrets_path,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': keycloak_realm,
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC_ID_TOKEN_COOKIE_SECURE': True,
})

oidc = OpenIDConnect(app)

valid_groups = set(json.loads(os.environ.get("GROUPS")))

cookies = {}
lovd_password = ''


def remove_login_links(content):
  content = content.decode('utf-8')
  content = re.sub(r'<[Aa]\s*href="login"\s*>[/<>"\w\s]*</[aA]>','',content)
  content = re.sub(r'<[Aa]\s*href="logout"\s*>[/<>"\w\s]*</[aA]>','',content)
  content = re.sub(r'<[Aa]\s*href="users\?register"\s*>[/<>"\w\s]*</[aA]>','',content)
  content = re.sub(r'<[Aa] href="submit">.*</[Aa]>','',content)
  content = re.sub(r'(?:(<[Ii][Mm][Gg] src="gfx/tab_BB.png".*>)\s*)+',r'\1',content)
  content = content.encode('utf-8')
  return content


@app.route('/logout', methods=['GET'])
def logout():
  oidc.logout()
  return flask.redirect(oidc.client_secrets['logout_uri']+'?'+
      urllib.parse.urlencode({'redirect_uri': flask.request.host_url}))

@app.route('/', defaults={'path': ''}, methods=["GET", "POST", "PUT", "DELETE"])
@app.route('/<path:path>', methods=["GET", "POST", "PUT", "DELETE"])
@oidc.require_login
def proxy(path):

  user_id = oidc.user_getfield('sub')
  group = ka.get_group(user_id, valid_groups)
  if not group:
    return "You don't belong to any group", 404
  lovd_url = f'http://lovd-{group}/'

  headers = {'Connection': 'Keep-Alive'}
  if path.find('login') >= 0:
    return 'You are not allowed to login.'
  if path.find('logout') >= 0:
    return 'You are not allowed to logout.'
  if flask.request.query_string:
    path = "%s?%s" % (path, flask.request.query_string.decode('utf-8'))
  try:
    if flask.request.method == 'GET':
      resp = requests.get(f'{lovd_url}{path}', cookies=cookies)
    elif flask.request.method == 'POST':
      form_data = dict(flask.request.form)
      if 'password' in form_data and not path.startswith('users'):
        form_data['password'] = lovd_password
      resp = requests.post(f'{lovd_url}{path}', cookies=cookies, data=form_data)
    elif flask.request.method == 'PUT':
      resp = requests.put(f'{lovd_url}{path}', cookies=cookies, data=flask.request.form)
    elif flask.request.method == 'DELETE':
      resp = requests.delete(f'{lovd_url}{path}', cookies=cookies, data=flask.request.form)
    else:
      return 'You are not allowed to perform this action.'
  except Exception as e:
    return f'Cannot connect to {lovd_url}. Error: {str(e)}', 404
  content = resp.content
  if resp.headers['content-type'].find('text') >= 0:
    content = content.replace(lovd_url.encode('utf-8'), proxy_url.encode('utf-8'))
  if resp.headers['content-type'].find('text/html') >= 0:
    content = remove_login_links(content)
  
  if resp.headers['content-type'].find('text/html') >= 0:
    headers['Cache-control'] = 'no-store, no-cache, must-revalidate'
  else:
    headers['Cache-control'] = 'max-age=86400'
  
  flask_response = flask.Response(response=content, headers=headers, content_type=resp.headers['content-type'])
  return flask_response


if __name__ == '__main__':
  app.run(host='localhost', port=5002, debug=True)